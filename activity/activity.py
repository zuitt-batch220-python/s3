# Activity
# Create a car dictionary with the following keys: brand, model, year of make, color (any values)

my_car = {
    "brand": "Mitsubishi",
    "model": "G4",
    "year_of_make": 2019,
    "color": "Silver",
}
# Print the following statement from the details: "I own a (color) (brand) (model) and it was made in (year_of_make)
print(f"I own a {my_car['color']} {my_car['brand']} {my_car['model']} and it was made in {my_car['year_of_make']}\n")


# Create a function that gets the square of a number
def square(number):
    return number ** 2


num = int(input("Enter a number: "))
result = (f"The square root of {num} is {square(num)}")
print(result)

# Create a function that takes one of the following languages as its parameter: a. French b. Spanish c. Japanese
# Depending on which language is given, the function prints "Hello World!" in that language. Add a condition that ask the user to input a valid language if the given parameter does not match any of the above.
print(
    "\nLet's translate 'Hello World!' to French, Spanish or Japanese languages only. Other languages are not yet available to our system.")
languages = ["French", "Spanish", "Japanese"]
preferred_language = input("Please enter your preferred language: ").capitalize()

if preferred_language in languages:
    if preferred_language == "French":
        print("Bonjour le monde!")
    elif preferred_language == "Spanish":
        print("Hola Mundo!")
    else:
        print("`Kon'nichiwa sekai'")
else:
    print("Sorry. Entered language is not yet available to our system")
