# Functions
# Functions are block of code that can be run when called/invoked. A function can be used to get input, process the input, and return output.

# The "def" keyword is used to create a function
def my_greeting():
    # code to be executed when function is invoked.
    print("Hello User!")


# Calling/invoking the function
my_greeting()


# Parameters can be added to functions to have more control as to what input the function will need
def greet_user(username):
    # print out the value of the username parameter
    print(f"Hello, {username}")


greet_user("Kim")


# Return statements - the "return" keyword allows functions to return values.

def addition(num1, num2):
    return num1 + num2


sum = addition(5, 10)
print(f"The sum is {sum}")

# Lamba Functions
# Lamba Functions is a small anonymous function that can be used for callbacks. It is just like any normal python function, except that its name is defined as a variable, and usually contains just one line of code.
# A lambda function can take any number of arguments, but can only have on expression
# Callback - function inside of a functions

greeting = lambda person: f'Hello {person}'
print(greeting("Anthony"))

mult = lambda a, b: a * b
print(mult(5, 2))


def increaser(num1):
    return lambda num2: num2 * num1


doubler = increaser(2)
print(doubler(11))

tripler = increaser(3)
print(tripler(7))
