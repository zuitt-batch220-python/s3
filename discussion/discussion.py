# [Section] Lists
# lists in Python is similar to the arrays in JS. They both provide a collection of data
names = ["John", "Paul", "George", "Ringo"]
programs = ["developer career", "pi-shape", "short-courses"]
durations = [360, 180, 20]
truth_values = [True, False, True, True, False]
print(names)

# list with different data types
# this is allowed in python, jiuust keep in mind the convention of having the same data type in each of the list.
sample_list = ["Apple", 3, False, "Potato", 4, True]
print(sample_list)
print(len(sample_list))

# accessing the values inside the list
print(durations[1])
print(programs[0])
print(names[-1])

print(names[0:3])

# Mini Activity
"""student_names = ["Kim", "Janis", "Lester", "Chit", "Carmel"]
student_grades = [88, 95, 90, 86, 75]

for (name, grade) in zip(student_names, student_grades):
    print(f"{name}'s grade is {grade}")
"""

# Updating Lists
# Print the current value
print(f"Current Value: {names[2]}")
names[2] = 'Michael'
print(f"New Value: {names[2]}")
print(names)

# Adding list Items - inserting at the end of the list
names.append("Bobby")
print(names)
durations.append(360)
print(durations)

# Delete item in a list
del durations[-1]
print(durations)

# Membership checks - the "in" keyword checks if a given elements is in the list and returns true or false
print(20 in durations)
print(500 in durations)
names.sort()
print(names)

# Looping through Lists
count = 0
while count < len(names):
    print(names[count])
    count += 1

# Dictionaries - use to store data in key:value pairs.
# Are collections which are ordered, changeable, and do not allow to duplicates.
# Ordered means that items have a defined order that cannot be changed
# Changeable means that value can be changed
person1 = {
    "name": "Brandon",
    "age": 28,
    "occupation": "student",
    "isEnrolled": True,
    "subjects": ["Python", "SQL", "Django"]
}

person2 = {
    "name": "Monika",
    "age": 28,
    "occupation": "student",
    "isEnrolled": True,
    "subjects": ["Python", "SQL", "Django"]
}

clasroom = {
    "student1": person1,
    "student2": person2
}

# To get the number of key:value pairs in a dictionary, the len() method can be used again.
print(len(person1))

# Accessing Values in Dictionaries
# To get the value of an item in a dictionary, the key name can be used with a pair of square brackets
print(person1["name"])

# The keus() and values() methods will return a list of all keys/values in the dictionary
print(person1.keys())
print(person1.values())

# The items() method will return each item in the dictionary as key:value pair in a list
print(person1.items())

# Adding key:value using by assign or update()method
person1["nationality"] = "Filipino"
person1.update({"fav_food": "BBQ"})
print(person1)

# Delete using pop()method or del keyword
person1.pop("fav_food")
del person1["nationality"]
print(person1)

# Looping through dictionaries
for key in person1:
    print(f"The value of {key} is {person1[key]}")

print(clasroom)
